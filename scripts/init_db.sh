#! /usr/bin/env bash

set -x
set -eo pipefail

if ! [ -x "$(command -v psql)" ]; then
    echo >&2 "Error: psql is not installed"
fi


cargo sqlx --version > /dev/null
if ! [ -x "$(command -v sqlx)"  ] ; then
    echo >&2 "Error: sqlx is not installed."
    echo >&2 "Use:"
    echo >&2 " cargo install sqlx-cli --no-default-features --features postgres,rustls"
    echo >&2 "to install it."
    exit 1
fi

DB_USER="${POSTGRES_USER:=postgres}"
DB_PASSWORD="${POSTGRES_PASSWORD:=password}"
DB_NAME="${POSTGRES_DB:=newsletter}"
DB_PORT="${POSTGRES_PORT:=5432}"
DB_HOST="${DB_HOST:=localhost}"

if [[ -z "${SKIP_DOCKER}" ]]; then
docker run \
    --rm \
    -e POSTGRES_USER=${DB_USER} \
    -e POSTGRES_DB=${DB_NAME} \
    -e POSTGRES_PASSWORD=${DB_PASSWORD} \
    -p "${DB_PORT}":5432 \
    -d postgres:14 \
    postgres -N 1000
fi

export PGPASSWORD="${DB_PASSWORD}"
until psql -h ${DB_HOST} -U "${DB_USER}" -p "${DB_PORT}" -d "postgres" -c "\q" ; do
    >&2 echo "Postgres is still unavailable - sleeping"
    sleep 1
done

>&2 echo "Postgres is up and running on port ${DB_PORT}"

export DATABASE_URL="postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}"
sqlx database create
sqlx migrate run

>&2 echo "Postgres has been migrated, ready to go"

if [[ -z "${SKIP_DOCKER}" ]]; then
    container=$(docker ps | rg postgres | cut -d' ' -f1)
    docker attach ${container}
fi
