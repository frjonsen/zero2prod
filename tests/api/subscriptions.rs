use crate::helpers::spawn_app;
use test_case::test_case;
use wiremock::matchers::{method, path};
use wiremock::{Mock, ResponseTemplate};

#[tokio::test]
async fn subscribe_returns_a_200_for_valid_form_data() {
    // Arrange
    let app = spawn_app().await;
    let body = "name=le%20guin&email=ursula_le_guin%40gmail.com";

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&app.email_server)
        .await;

    // Act
    let response = app.post_subscriptions(body.into()).await;

    // Assert
    assert_eq!(reqwest::StatusCode::OK, response.status());
}

#[tokio::test]
async fn subscribe_persists_the_new_subscriber() {
    // Arrange
    let app = spawn_app().await;
    let body = "name=le%20guin&email=ursula_le_guin%40gmail.com";

    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .mount(&app.email_server)
        .await;

    // Act
    app.post_subscriptions(body.into()).await;

    //Assert
    let saved = sqlx::query!("SELECT email, name, status FROM subscriptions")
        .fetch_one(&app.db_pool)
        .await
        .expect("Failed to fetch saved subscriptions");

    assert_eq!(saved.email, "ursula_le_guin@gmail.com");
    assert_eq!(saved.name, "le guin");
    assert_eq!(saved.status, "pending_confirmation");
}

#[test_case("name=le%20guin" ; "missing the email")]
#[test_case("email=ursula_le_guin%40gmail.com" ; "missing the name")]
#[test_case("" ; "missing both name and email")]
#[tokio::test]
async fn subscribe_returns_a_400_when_data_is_missing(invalid_body: &'static str) {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app.post_subscriptions(invalid_body.into()).await;

    // Assert
    assert_eq!(reqwest::StatusCode::BAD_REQUEST, response.status())
}

#[test_case("name=Ursula&email=definitely-not-an-email" ; "invalid email")]
#[tokio::test]
async fn subscribe_returns_a_400_when_fields_are_present_but_empty(invalid_body: &'static str) {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app.post_subscriptions(invalid_body.into()).await;

    // Assert
    assert_eq!(
        reqwest::StatusCode::BAD_REQUEST,
        response.status(),
        "The API did not return a 400 Bad Request"
    )
}

#[tokio::test]
async fn subscribe_sends_a_confirmation_email_with_a_link() {
    // Arrange
    let app = spawn_app().await;
    let body = "name=le%20guin&email=ursula_le_guin%40gmail.com";
    Mock::given(path("/email"))
        .and(method("POST"))
        .respond_with(ResponseTemplate::new(200))
        .expect(1)
        .mount(&app.email_server)
        .await;

    // Act
    let response = app.post_subscriptions(body.into()).await;

    // Assert
    assert_eq!(reqwest::StatusCode::OK, response.status());

    let email_request = &app.email_server.received_requests().await.unwrap()[0];
    let confirmation_links = app.get_confirmation_links(email_request);

    assert_eq!(confirmation_links.html, confirmation_links.plain_text);
}

#[tokio::test]
async fn subscribe_fails_if_there_is_a_fatal_database_error() {
    // Arrange
    let app = spawn_app().await;
    let body = "name=le%20guin&email=ursula_le_guin%40gmail.com";
    sqlx::query!("ALTER TABLE subscription_tokens DROP COLUMN subscription_token")
        .execute(&app.db_pool)
        .await
        .unwrap();

    let response = app.post_subscriptions(body.into()).await;
    assert_eq!(
        reqwest::StatusCode::INTERNAL_SERVER_ERROR,
        response.status()
    )
}
