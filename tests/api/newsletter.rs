use std::time::Duration;

use crate::helpers::{assert_is_redirect_to, spawn_app, ConfirmationLinks, TestApp};
use fake::faker::lorem::en::{Paragraph, Sentence};
use fake::Fake;
use serde_json::Value;
use test_case::test_case;
use wiremock::matchers::{any, method, path};
use wiremock::{Mock, ResponseTemplate};

use fake::faker::internet::en::SafeEmail;
use fake::faker::name::en::Name;
use wiremock::MockBuilder;

fn when_sending_an_email() -> MockBuilder {
    Mock::given(path("/email")).and(method("POST"))
}

#[test_case(serde_json::json!({"title": "asd", "html_text": "<p>a</p>"}) ; "no plain text content")]
#[test_case(serde_json::json!({"title": "asd", "plain_text": "a"}) ; "no html text content")]
#[test_case(serde_json::json!({"html_text": "<p>a</p>", "plain_text": "a"}) ; "no title")]
#[tokio::test]
async fn newsletters_returns_400_for_invalid_data(invalid_body: serde_json::Value) {
    // Arrange
    let app = spawn_app().await;

    // Act - Part 1 - Login
    let response = app.post_valid_login().await;
    assert_is_redirect_to(&response, "/admin/dashboard");

    // Act - Part 2 - Send newsletter
    let response = app.post_newsletters(&invalid_body).await;

    // Assert
    assert_eq!(reqwest::StatusCode::BAD_REQUEST, response.status())
}

#[tokio::test]
async fn newsletters_are_not_delivered_to_unconfirmed_subscribers() {
    // Arrange
    let app = spawn_app().await;
    create_unconfirmed_subscriber(&app).await;

    Mock::given(any())
        .respond_with(ResponseTemplate::new(200))
        .expect(0)
        .mount(&app.email_server)
        .await;

    // Act - Part 1 - Login
    let response = app
        .post_login(&serde_json::json!({
            "username": app.test_user.username,
            "password": app.test_user.password
        }))
        .await;
    assert_is_redirect_to(&response, "/admin/dashboard");

    // Act - Part 2 - Send newsletter
    let response = app.post_newsletters(&generate_newsletter_json_body()).await;

    // Assert
    assert_is_redirect_to(&response, "/admin/newsletters");

    // Act - Part 3 - Follow redirect
    let response = app.get_newsletter_form_html().await;

    // Assert
    assert!(response.contains("has been accepted"));
    app.dispatch_all_pending_emails().await;
}

#[tokio::test]
async fn newsletters_are_delivered_to_confirmed_subscribers() {
    let app = spawn_app().await;
    create_confirm_subscriber(&app).await;

    when_sending_an_email()
        .respond_with(ResponseTemplate::new(200))
        .expect(1)
        .mount(&app.email_server)
        .await;

    // Act - Part 1 - Login
    app.test_user.login(&app).await;

    // Act - Part 2 - Send newsletter
    let response = app.post_newsletters(&generate_newsletter_json_body()).await;

    // Assert
    assert_is_redirect_to(&response, "/admin/newsletters");

    // Act - Part 3 - Follow redirect
    let response = app.get_newsletter_form_html().await;
    assert!(response.contains("has been accepted"));

    app.dispatch_all_pending_emails().await;
}

#[tokio::test]
async fn newsletter_redirects_if_not_logged_in() {
    // Arrange
    let app = spawn_app().await;

    // Act
    let response = app.get_newsletter_form().await;

    // Assert
    assert_is_redirect_to(&response, "/login")
}

async fn create_unconfirmed_subscriber(app: &TestApp) -> ConfirmationLinks {
    let name: String = Name().fake();
    let email: String = SafeEmail().fake();
    let body = serde_urlencoded::to_string(&serde_json::json!({
        "name": name,
        "email": email
    }))
    .unwrap();

    let _mock_guard = when_sending_an_email()
        .respond_with(ResponseTemplate::new(200))
        .named("Create unconfirmed subscriber")
        .expect(1)
        .mount_as_scoped(&app.email_server)
        .await;

    app.post_subscriptions(body)
        .await
        .error_for_status()
        .unwrap();

    let email_request = &app
        .email_server
        .received_requests()
        .await
        .unwrap()
        .pop()
        .unwrap();
    app.get_confirmation_links(email_request)
}

async fn create_confirm_subscriber(app: &TestApp) {
    let confirmation_link = create_unconfirmed_subscriber(app).await;
    reqwest::get(confirmation_link.html)
        .await
        .unwrap()
        .error_for_status()
        .unwrap();
}

fn generate_newsletter_json_body() -> Value {
    let title = Sentence(1..2);
    let body = Paragraph(2..3);
    let key = (1..=51).fake::<String>();
    serde_json::json!({
        "idempotency_key": key,
        "title": title.fake::<String>(),
        "plain_text": body.fake::<String>(),
        "html_text": format!("<html>{}</html>", body.fake::<String>())
    })
}

#[tokio::test]
async fn requests_missing_authorization_are_rejected() {
    // Arrange
    let app = spawn_app().await;
    let response = app.post_newsletters(&generate_newsletter_json_body()).await;

    assert_is_redirect_to(&response, "/login")
}

#[tokio::test]
async fn newsletter_creation_is_idempotent() {
    // Arrange
    let app = spawn_app().await;
    create_confirm_subscriber(&app).await;
    app.test_user.login(&app).await;

    when_sending_an_email()
        .respond_with(ResponseTemplate::new(200))
        .expect(1)
        .mount(&app.email_server)
        .await;

    let newsletter_request_body = serde_json::json!({
        "title": "Newsletter title",
        "plain_text": "Newsletter body as plain text",
        "html_text": "<p>News letter body as HTML</p>",
        // We expect the idempotency key as part of the form data,
        // not has a header
        "idempotency_key": uuid::Uuid::new_v4().to_string()
    });

    // Act - Part 1 - Publish newsletter
    let response = app.post_newsletters(&newsletter_request_body).await;
    assert_is_redirect_to(&response, "/admin/newsletters");

    // Act - Part 2 - Follow redirect
    let html_page = app.get_newsletter_form_html().await;
    assert!(html_page.contains("has been accepted"));

    // Act - Part 3 - Submit same newsletter
    let response = app.post_newsletters(&newsletter_request_body).await;
    assert_is_redirect_to(&response, "/admin/newsletters");

    // Act - Part 4 - Follow redirect
    let html_page = app.get_newsletter_form_html().await;
    assert!(html_page.contains("has been accepted"));

    app.dispatch_all_pending_emails().await;
}

#[tokio::test]
async fn concurrent_form_submission_is_handled_gracefully() {
    // Arrange
    let app = spawn_app().await;
    create_confirm_subscriber(&app).await;
    app.test_user.login(&app).await;

    when_sending_an_email()
        .respond_with(ResponseTemplate::new(200).set_delay(Duration::from_secs(2)))
        .expect(1)
        .mount(&app.email_server)
        .await;

    // Act - Submit two newsletter forms concurrently
    let newsletter_body = generate_newsletter_json_body();
    let response1 = app.post_newsletters(&newsletter_body);
    let response2 = app.post_newsletters(&newsletter_body);
    let (response1, response2) = tokio::join!(response1, response2);

    // Assert
    assert_eq!(response1.status(), response2.status());
    assert_eq!(
        response1.text().await.unwrap(),
        response2.text().await.unwrap()
    );

    app.dispatch_all_pending_emails().await;
}
