#! /usr/bin/env python3
import os
import sys
from typing import Any, Dict, Optional, Union
import json
from urllib.request import urlopen, Request
from pathlib import Path
import subprocess
from functools import partial
from datetime import datetime

FALLBACK_HOME = Path.home() / '.local' / 'share'
XDG_DATA_HOME = os.environ.get('XDG_DATA_HOME')
DATA_HOME = Path(XDG_DATA_HOME) if XDG_DATA_HOME is not None else FALLBACK_HOME
BUILDER_DATA = DATA_HOME / 'zero2prod-ci-builder' / 'versions'
docker_token: Union[str, None] = None
run = partial(subprocess.run, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf-8')

DEFAULT_DOCKERFILE = Path('Dockerfile')
DOCKERFILE_PATH = os.environ.get('DOCKERFILE_PATH')

if DOCKERFILE_PATH is None:
    DOCKERFILE_PATH = DEFAULT_DOCKERFILE


DOCKER_REPO = os.environ.get('DOCKER_REPO', 'vakz')

def get_docker_hub_token() -> str:
    global docker_token
    if docker_token is not None:
        return docker_token
    

    url = "https://auth.docker.io/token?service=registry.docker.io&scope=repository:library/rust:pull"

    token_res = make_http_request(url)
    docker_token = token_res['token']

    assert docker_token
    return docker_token



def get_latest_rust_image_digest() -> str:
    accept = 'application/vnd.docker.distribution.manifest.list.v2+json'
    token = get_docker_hub_token()

    headers = {'Accept': accept, 'Authorization': f'Bearer {token}'}
    digest_res = make_http_request('https://registry-1.docker.io/v2/library/rust/manifests/latest', headers)
    manifests = digest_res['manifests']

    amd64_manifest = next(filter(lambda m: m['platform']['architecture'] == 'amd64', manifests))

    return amd64_manifest['digest']




def make_http_request(url: str, headers: Optional[Dict[str, str]] = None) -> Dict[str, Any]:
    user_agent = os.environ.get('CRATES_IO_BOT_UA', 'frjonsen-ci-builder (fredrik@jonsen.se)')
    _headers = {
        'User-Agent': user_agent,
        'Accept': 'application/json'
    }

    if headers is not None:
        _headers.update(headers)


    req = Request(url, headers=_headers)

    with urlopen(req) as response:
        body = response.read().decode()
        if response.status != 200:
            print(body)
            raise Exception(f'Failed to make request to {url}')

        return json.loads(body)

        

def get_newest_versions() -> Dict[str, str]:
    github_base = 'https://rust-lang.github.io/rustup-components-history/x86_64-unknown-linux-gnu/'
    crates_base = 'https://crates.io/api/v1/crates/'

    clippy_res = make_http_request(f'{github_base}/clippy.json')
    rustfmt_res = make_http_request(f'{github_base}/rustfmt.json')
    tarpaulin_res = make_http_request(f'{crates_base}/cargo-tarpaulin')
    audit_res = make_http_request(f'{crates_base}/cargo-audit')
    sqlx_res = make_http_request(f'{crates_base}/sqlx-cli')
    rust_image_version = get_latest_rust_image_digest()



    return {
        'clippy': clippy_res['last_available'],
        'rustfmt': rustfmt_res['last_available'],
        'tarpaulin': tarpaulin_res['crate']['max_stable_version'],
        'audit': audit_res['crate']['max_stable_version'],
        'sqlx': sqlx_res['crate']['max_stable_version'],
        'rust': rust_image_version
    }

def get_cached_versions() -> Dict[str, str]:
    if not BUILDER_DATA.exists():
        return {}
    return json.loads(BUILDER_DATA.read_text())

def is_already_latest() -> bool:
    last_used = get_cached_versions()
    newest_available = get_newest_versions()

    return last_used == newest_available

def build_new_image() -> None:
    tag = datetime.now().strftime('%y.%m.%dT%H.%M')
    image_name = f'{DOCKER_REPO}/zero2prod-ci:{tag}'
    print('Building new image')
    build_call = subprocess.Popen(['docker', 'build', '-f', DOCKERFILE_PATH, '-t', image_name, '.'], stderr=subprocess.STDOUT, stdout=subprocess.PIPE, encoding='utf-8')
    while build_call.poll() is None:
        print(build_call.stdout.readline(), end='')
    if build_call.returncode != 0:
        print('Building image failed')
        sys.exit(1)
    print('Pushing image to Docker Hub')
    push_call = run(['docker', 'push', image_name])
    if push_call.returncode != 0:
        print("Push failed")
        print(push_call.stdout)


def main() -> None:
    old_versions = get_cached_versions()
    new_versions = get_newest_versions()

    if old_versions == new_versions:
        print('Nothing has changed since last build. Exiting early.')
        sys.exit(0)

    if old_versions.get('rust') != new_versions.get('rust'):
        res = run(['docker', 'pull', 'rust:latest'])
        if res.returncode != 0:
            print("Pulling latest rust image failed")
            sys.exit(1)

    print('Changes detected')
    print('Old versions:\n', old_versions)
    print('New versions:\n', new_versions)
    build_new_image()

    if not BUILDER_DATA.parent.exists():
        os.makedirs(BUILDER_DATA.parent, exist_ok=True)
    BUILDER_DATA.write_text(json.dumps(new_versions))


main()