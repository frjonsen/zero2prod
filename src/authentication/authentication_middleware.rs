use std::future::{ready, Future, Ready};
use std::ops::Deref;
use std::pin::Pin;
use std::rc::Rc;

use actix_web::error::InternalError;
use actix_web::{Error, FromRequest, HttpMessage};

use actix_web::dev::{Service, ServiceRequest, ServiceResponse, Transform};
use uuid::Uuid;

use crate::session_state::TypedSession;
use crate::utils::{e500, see_other};

pub struct Authentication;

impl<S: 'static, B> Transform<S, ServiceRequest> for Authentication
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
    B: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = Error;
    type InitError = ();
    type Transform = AuthenticationMiddleware<S>;
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ready(Ok(AuthenticationMiddleware {
            service: Rc::new(service),
        }))
    }
}

pub struct AuthenticationMiddleware<S> {
    service: Rc<S>,
}

#[derive(Copy, Clone, Debug)]
pub struct UserId(Uuid);

impl std::fmt::Display for UserId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl Deref for UserId {
    type Target = Uuid;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<S, B> Service<ServiceRequest> for AuthenticationMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
    S::Future: 'static,
    B: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>> + 'static>>;

    actix_web::dev::forward_ready!(service);

    fn call(&self, mut req: ServiceRequest) -> Self::Future {
        let svc = self.service.clone();
        Box::pin(async move {
            let session = {
                let (http_request, payload) = req.parts_mut();
                TypedSession::from_request(http_request, payload)
            }
            .await?;

            match session.get_user_id().map_err(e500)? {
                Some(user_id) => {
                    req.extensions_mut().insert(UserId(user_id));
                }
                None => {
                    let response = see_other("/login");
                    let e = anyhow::anyhow!("The user has not logged in");
                    return Err(InternalError::from_response(e, response).into());
                }
            }

            let fut = svc.call(req).await?;
            Ok(fut)
        })
    }
}
