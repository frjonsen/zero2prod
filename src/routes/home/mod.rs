use actix_web::{http::header::ContentType, HttpResponse};

mod login;
pub use login::*;

pub async fn home() -> HttpResponse {
    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(include_str!("home.html"))
}
