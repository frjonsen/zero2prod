use std::fmt::Write;

use actix_web::{http::header::ContentType, HttpResponse};
use actix_web_flash_messages::IncomingFlashMessages;

pub async fn get_newsletter_form(flash_messages: IncomingFlashMessages) -> HttpResponse {
    let idempotency_key = uuid::Uuid::new_v4().to_string();
    let mut errors = String::new();
    for e in flash_messages.iter() {
        writeln!(errors, "<p><i>{}</i></p>", e.content()).unwrap();
    }
    HttpResponse::Ok()
        .content_type(ContentType::html())
        .body(format!(
            include_str!("newsletter.html"),
            errors, idempotency_key
        ))
}
