use std::time::Duration;

use crate::{
    configuration::Settings, domain::SubscriberEmail, email_client::EmailClient,
    startup::get_connection_pool,
};
use sqlx::{PgPool, Postgres, Transaction};
use tracing::{field::display, Span};
use uuid::Uuid;

pub enum ExecutionOutcome {
    TaskCompleted,
    EmptyQueue,
}

struct Subscriber {
    subscriber_id: Uuid,
    email: String,
}

struct NewsletterIssue {
    title: String,
    plain_text: String,
    html_text: String,
}

pub async fn run_worker_until_stopped(configuration: Settings) -> Result<(), anyhow::Error> {
    let connection_pool = get_connection_pool(&configuration.database);
    let email_client = configuration.email_client.client();
    worker_loop(connection_pool, email_client).await
}

async fn worker_loop(pool: PgPool, email_client: EmailClient) -> Result<(), anyhow::Error> {
    loop {
        match try_execute_task(&pool, &email_client).await {
            Ok(ExecutionOutcome::EmptyQueue) => {
                tokio::time::sleep(Duration::from_secs(10)).await;
            }
            Ok(ExecutionOutcome::TaskCompleted) => {}
            Err(_) => {
                tokio::time::sleep(Duration::from_secs(1)).await;
            }
        }
    }
}

#[tracing::instrument(skip_all)]
async fn get_issue(pool: &PgPool, issue_id: Uuid) -> Result<NewsletterIssue, anyhow::Error> {
    let issue = sqlx::query_as!(
        NewsletterIssue,
        r#"
    SELECT title, text_content AS plain_text, html_content AS html_text
    FROM newsletter_issues
    WHERE newsletter_issue_id = $1
    "#,
        issue_id
    )
    .fetch_one(pool)
    .await?;
    Ok(issue)
}

#[tracing::instrument(skip_all, fields(
    newsletter_issue_id=tracing::field::Empty,
    subscriber_email=tracing::field::Empty),
    err
)]
pub async fn try_execute_task(
    pool: &PgPool,
    email_client: &EmailClient,
) -> Result<ExecutionOutcome, anyhow::Error> {
    let task = dequeue_task(pool).await?;
    if task.is_none() {
        return Ok(ExecutionOutcome::EmptyQueue);
    }
    let (transaction, issue_id, subscriber) = task.unwrap();
    Span::current()
        .record("newsletter_issue_id", &display(issue_id))
        .record("subscriber_email", &display(subscriber.email.clone()));
    match SubscriberEmail::parse(subscriber.email) {
        Ok(email) => {
            let issue = get_issue(pool, issue_id).await?;
            let email_result = email_client
                .send_email(&email, &issue.title, &issue.html_text, &issue.plain_text)
                .await;

            if let Err(e) = email_result {
                tracing::error!(
                    error.cause_chain = ?e,
                    error.message = %e,
                    "Failed to deliver issue to a confirmed subscriber. Skipping."
                )
            }
        }
        Err(e) => {
            tracing::error!(
                error.cause_chain = ?e,
                error.message = %e,
                "Skipping a confirmed subscriber. Their stored contact details are invalid"
            );
        }
    }
    delete_task(transaction, issue_id, subscriber.subscriber_id).await?;

    Ok(ExecutionOutcome::TaskCompleted)
}

type PgTransaction = Transaction<'static, Postgres>;

#[tracing::instrument(skip_all)]
async fn dequeue_task(
    pool: &PgPool,
) -> Result<Option<(PgTransaction, Uuid, Subscriber)>, anyhow::Error> {
    let mut transaction = pool.begin().await?;
    let task = sqlx::query!(
        r#"
        SELECT q.newsletter_issue_id, s.id AS subscriber_id, s.email AS subscriber_email
        FROM issue_delivery_queue q
        INNER JOIN subscriptions s
        ON q.subscriber_id = s.id
        FOR UPDATE
        SKIP LOCKED
        LIMIT 1
        "#
    )
    .fetch_optional(&mut transaction)
    .await?
    .map(|r| {
        (
            transaction,
            r.newsletter_issue_id,
            Subscriber {
                email: r.subscriber_email,
                subscriber_id: r.subscriber_id,
            },
        )
    });

    Ok(task)
}

async fn delete_task(
    mut transaction: PgTransaction,
    issue_id: Uuid,
    subscriber_id: Uuid,
) -> Result<(), anyhow::Error> {
    sqlx::query!(
        r#"
    DELETE FROM issue_delivery_queue
    WHERE
        newsletter_issue_id = $1 AND
        subscriber_id = $2
    "#,
        issue_id,
        subscriber_id
    )
    .execute(&mut transaction)
    .await?;

    transaction.commit().await?;
    Ok(())
}
